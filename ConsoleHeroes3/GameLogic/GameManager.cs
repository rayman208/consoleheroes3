﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleHeroes3.Entities;
using ConsoleHeroes3.Tools;

namespace ConsoleHeroes3.GameLogic
{
    class GameManager
    {
        private Player playerLeft;
        private Player playerRight;
        private BattleField battleField;

        public GameManager()
        {
            Settings.Init();
            battleField = new BattleField();
        }

        private Player InitPlayer(ConsoleColor color)
        {
            string name = ConsoleHelper.InputString("Введите имя игрока: ");

            int money = ConsoleHelper.InputIntInRange("Введите деньги игрока (30..250): ", 30, 250);

            return new Player(name, money, color);
        }

        private void BuyUnitsForPlayer(Player player)
        {
            while (true)
            {
                Console.Clear();
                player.PrintPersonalInfo();
                player.PrintUnitsInfo();

                Console.WriteLine("Меню:");
                Console.WriteLine("1. Купить юнита");
                Console.WriteLine("2. Продать юнита");
                Console.WriteLine("0. Закончить покупку");

                int action, position;
                action = ConsoleHelper.InputIntInRange("Введите действие: ", 0, 2);

                switch (action)
                {
                    case 1:
                        Console.WriteLine("Доступные для покупки юниты:");

                        Console.WriteLine("1");
                        Console.WriteLine(Archer.GetInfo());
                        Console.WriteLine("---");

                        Console.WriteLine("2");
                        Console.WriteLine(Knight.GetInfo());
                        Console.WriteLine("---");

                        Console.WriteLine("3");
                        Console.WriteLine(Wizard.GetInfo());
                        Console.WriteLine("---");

                        position = ConsoleHelper.InputIntInRange("Введите позицию для покупки: ", 1, 3);

                        bool isBought = false;

                        switch (position)
                        {
                            case 1:
                                if (player.Money >= Archer.GetCost())
                                {
                                    player.AddUnit(new Archer());
                                    isBought = true;
                                }
                                break;
                            case 2:
                                if (player.Money >= Knight.GetCost())
                                {
                                    player.AddUnit(new Knight());
                                    isBought = true;
                                }
                                break;
                            case 3:
                                if (player.Money >= Wizard.GetCost())
                                {
                                    player.AddUnit(new Wizard());
                                    isBought = true;
                                }
                                break;
                        }

                        if (isBought == true)
                        {
                            Console.WriteLine("Покупка прошла успешна");
                        }
                        else
                        {
                            Console.WriteLine("У Вас недостаточно денег");
                        }

                        ConsoleHelper.WaitingEnter();

                        break;

                    case 2:
                        if (player.CountUnits == 0)
                        {
                            break;
                        }

                        position = ConsoleHelper.InputIntInRange("Введите номер юнита для продажи: ", 1,
                            player.CountUnits);
                        player.RemoveUnit(position);
                        break;

                    case 0:
                        if (player.CountUnits == 0)
                        {
                            break;
                        }
                        return;
                }
            }
        }

        private void PlaceUnitsForPlayer(Player player, int minX, int maxX)
        {
            for (int i = 0; i < player.CountUnits; i++)
            {
                Console.Clear();

                player.DrawUnits();

                Console.ResetColor();

                battleField.Draw();

                Console.WriteLine();

                Console.WriteLine("Поставьте на поле юнита: ");
                Console.WriteLine(player.GetUnitByIndex(i).GetInfo());

                int x, y;
                Unit unitInCoord;
                do
                {
                    unitInCoord = null;
                    x = ConsoleHelper.InputIntInRange($"Введите X ({minX}...{maxX}): ", minX, maxX);
                    y = ConsoleHelper.InputIntInRange($"Введите Y (1...{battleField.Height}): ", 1, battleField.Height);

                    unitInCoord = player.GetUnitByXY(x, y);
                }
                while (unitInCoord != null);

                player.GetUnitByIndex(i).SetXY(x, y);
            }

            Console.Clear();

            player.DrawUnits();

            Console.ResetColor();

            battleField.Draw();

            Console.WriteLine();

            ConsoleHelper.WaitingEnter();
        }

        private void PrintBattleFieldWithPlayers()
        {
            playerLeft.DrawUnits();
            playerRight.DrawUnits();

            Console.ResetColor();
            battleField.Draw();

            Console.WriteLine();
        }

        private bool BattleStep(Player attackPlayer, Player defendPlayer)
        {
            for (int i = 0; i < attackPlayer.CountUnits; i++)
            {
                Unit currentUnit = attackPlayer.GetUnitByIndex(i);
                int x, y;
                bool canAction;

                Console.Clear();
                PrintBattleFieldWithPlayers();

                Console.WriteLine($"Ход игрока {attackPlayer.Name}");
                Console.WriteLine($"Ходит существо: {currentUnit.GetShortInfo()}");

                Console.WriteLine("1. Ходить");
                Console.WriteLine("2. Атаковать");
                Console.WriteLine("3. Пропустить ход");
                int action = ConsoleHelper.InputIntInRange("Введите номер действия: ", 1, 3);

                switch (action)
                {
                    case 1:
                        do
                        {
                            canAction = true;

                            x = ConsoleHelper.InputIntInRange("Введите X: ", 1, battleField.Width);
                            y = ConsoleHelper.InputIntInRange("Введите Y: ", 1, battleField.Height);

                            if (currentUnit.CanStep(x, y) == false)
                            {
                                canAction = false;
                                Console.WriteLine("Это слишком далеко. Пожалуйста, выберите клетку ближе");
                                continue;
                            }

                            if (attackPlayer.GetUnitByXY(x, y) != null || defendPlayer.GetUnitByXY(x, y) != null)
                            {
                                canAction = false;
                                Console.WriteLine("Эта клетка уже заннята. Пожалуйста, выберите свободную клетку");
                                continue;
                            }
                        }
                        while (canAction == false);

                        currentUnit.SetXY(x, y);
                        currentUnit.StepDone();

                        break;
                    case 2:
                        do
                        {
                            canAction = true;

                            x = ConsoleHelper.InputIntInRange("Введите X: ", 1, battleField.Width);
                            y = ConsoleHelper.InputIntInRange("Введите Y: ", 1, battleField.Height);

                            if (currentUnit.CanAttack(x, y) == false)
                            {
                                canAction = false;
                                Console.WriteLine("Это слишком далеко. Пожалуйста, выберите юнита ближе");
                                continue;
                            }

                            if (defendPlayer.GetUnitByXY(x, y) == null)
                            {
                                canAction = false;
                                Console.WriteLine("В этой клетке нет врага. Пожалуйста, выберите клетку с вражеским юнитом");
                                continue;
                            }

                        } while (canAction == false);

                        Unit defendUnit = defendPlayer.GetUnitByXY(x, y);
                        currentUnit.Fight(defendUnit);
                        break;
                    case 3:
                        currentUnit.StepDone();
                        break;
                }

                attackPlayer.DeleteDeadUnits();
                defendPlayer.DeleteDeadUnits();

                if (defendPlayer.CountUnits == 0 || attackPlayer.CountUnits == 0)
                {
                    return false;
                }
            }

            return true;
        }


        public void InitPlayers()
        {
            Console.Clear();
            Console.WriteLine("Введите данные левого игрока: ");
            playerLeft = InitPlayer(ConsoleColor.DarkRed);

            Console.Clear();
            Console.WriteLine("Введите данные правого игрока: ");
            playerRight = InitPlayer(ConsoleColor.DarkBlue);

            Console.Clear();
            Console.WriteLine("Проверка введённых данных: ");

            Console.ForegroundColor = playerLeft.Color;
            Console.WriteLine("Левый игрок: ");
            playerLeft.PrintPersonalInfo();

            Console.ForegroundColor = playerRight.Color;
            Console.WriteLine("Правый игрок: ");
            playerRight.PrintPersonalInfo();

            Console.ResetColor();
            ConsoleHelper.WaitingEnter();
        }

        public void BuyUnitsForPlayers()
        {
            Console.Clear();
            Console.WriteLine($"Покупка юнитов для {playerLeft.Name}");
            ConsoleHelper.WaitingEnter();

            BuyUnitsForPlayer(playerLeft);

            Console.Clear();
            Console.WriteLine($"Покупка юнитов для {playerRight.Name}");
            ConsoleHelper.WaitingEnter();

            BuyUnitsForPlayer(playerRight);
        }

        public void PlaceUnitsForPlayers()
        {
            int minX, maxX;

            Console.Clear();
            Console.ResetColor();
            Console.WriteLine($"Расстановка юнитов для {playerLeft.Name}");
            ConsoleHelper.WaitingEnter();

            minX = 1;
            maxX = battleField.Width / 2;
            PlaceUnitsForPlayer(playerLeft, minX, maxX);

            Console.Clear();
            Console.ResetColor();
            Console.WriteLine($"Расстановка юнитов для {playerRight.Name}");
            ConsoleHelper.WaitingEnter();

            minX = battleField.Width / 2 + 1;
            maxX = battleField.Width;
            PlaceUnitsForPlayer(playerRight, minX, maxX);
        }

        public void Battle()
        {
            bool playGame = true;

            while (playGame == true)
            {
                Console.Clear();
                Console.ResetColor();

                Console.WriteLine($"Атакует {playerLeft.Name}, Защищается {playerRight.Name}");
                ConsoleHelper.WaitingEnter();

                playGame = BattleStep(playerLeft, playerRight);
                if (playGame == false) { continue; }

                Console.Clear();
                PrintBattleFieldWithPlayers();

                ConsoleHelper.WaitingEnter();

                Console.Clear();
                Console.ResetColor();

                Console.WriteLine($"Атакует {playerRight.Name}, Защищается {playerLeft.Name}");
                ConsoleHelper.WaitingEnter();

                playGame = BattleStep(playerRight, playerLeft);
                if (playGame == false) { continue; }

                Console.Clear();
                PrintBattleFieldWithPlayers();

                ConsoleHelper.WaitingEnter();

                playerLeft.ActivateUnits();
                playerRight.ActivateUnits();
            }
        }

        public void PrintWinner()
        {
            Console.Clear();
            Console.ResetColor();

            PrintBattleFieldWithPlayers();

            if (playerLeft.CountUnits == 0)
            {
                Console.WriteLine($"Победил {playerRight.Name}");
            }
            else if (playerRight.CountUnits == 0)
            {
                Console.WriteLine($"Победил {playerLeft.Name}");
            }
        }

    }
}
