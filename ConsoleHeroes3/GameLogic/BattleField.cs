﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleHeroes3.Tools;

namespace ConsoleHeroes3.GameLogic
{
    class BattleField
    {
        private int width;
        private int height;

        public BattleField()
        {
            width = Settings.GetIntValue("field_width");
            height = Settings.GetIntValue("field_height");
        }

        public int Width
        {
            get { return width; }
        }

        public int Height
        {
            get { return height; }
        }

        public void Draw()
        {
            for (int x = 0; x < width + 2; x++)
            {
                Console.SetCursorPosition(x, 0);
                Console.Write("*");

                Console.SetCursorPosition(x, height + 1);
                Console.Write("*");
            }

            for (int y = 0; y < height + 2; y++)
            {
                Console.SetCursorPosition(0, y);
                Console.Write("*");

                Console.SetCursorPosition(width + 1, y);
                Console.Write("*");
            }
        }
    }
}
