﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleHeroes3.Entities;

namespace ConsoleHeroes3.GameLogic
{
    class Player
    {
        private string name;
        private int money;
        private List<Unit> units;
        private ConsoleColor color;

        public Player(string name, int money, ConsoleColor color)
        {
            this.name = name;
            this.money = money;
            this.color = color;
            this.units = new List<Unit>();
        }

        public int CountUnits
        {
            get { return units.Count; }
        }

        public ConsoleColor Color
        {
            get { return color; }
        }

        public int Money
        {
            get { return money; }
        }

        public string Name
        {
            get { return name; }
        }

        public Unit GetUnitByIndex(int index)
        {
            return units[index];
        }

        public void AddUnit(Unit unit)
        {
            money -= unit.Cost;
            units.Add(unit);
        }

        public void RemoveUnit(int position)
        {
            int index = position - 1;
            money += units[index].Cost;
            units.RemoveAt(index);
        }

        public void PrintUnitsInfo()
        {
            if (units.Count == 0)
            {
                Console.WriteLine("Список юнитов пуст");
                return;
            }
                
            for (int i = 0; i < units.Count; i++)
            {
                Console.WriteLine($"{i+1}:\n"+units[i].GetInfo());
                Console.WriteLine("---");
            }
        }

        public void DrawUnits()
        {
            for (int i = 0; i < units.Count; i++)
            {
                units[i].Draw(color);
            }
        }

        public Unit GetUnitByXY(int x,int y)
        {
            return units.Find(unit => unit.X == x && unit.Y == y);
        }

        public void PrintPersonalInfo()
        {
            Console.WriteLine($"Имя: {name}\nКол-во монет: {money}");
            Console.WriteLine("---");
        }

        public void DeleteDeadUnits()
        {
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i].IsAlive == false)
                {
                    units.RemoveAt(i);
                    i--;
                }
            }
        }

        public void ActivateUnits()
        {
            for (int i = 0; i < units.Count; i++)
            {
                units[i].ResetStep();
            }
        }
    }
}
