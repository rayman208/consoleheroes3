﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleHeroes3.GameLogic;

namespace ConsoleHeroes3
{
    class Program
    {
        static void Main(string[] args)
        {
            GameManager gameManager = new GameManager();

            gameManager.InitPlayers();
            gameManager.BuyUnitsForPlayers();
            gameManager.PlaceUnitsForPlayers();
            gameManager.Battle();
            gameManager.PrintWinner();

            Console.ReadKey();
        }
    }
}
