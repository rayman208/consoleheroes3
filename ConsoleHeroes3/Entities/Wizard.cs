﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleHeroes3.Tools;

namespace ConsoleHeroes3.Entities
{
    class Wizard:Unit
    {
        public Wizard() : base(
            Settings.GetStringValue("wizard_name"),
            Settings.GetIntValue("wizard_hp"),
            Settings.GetIntValue("wizard_damage"),
            Settings.GetCharValue("wizard_view"),
            Settings.GetIntValue("wizard_stepDistance"),
            Settings.GetIntValue("wizard_attackDistance"),
            Settings.GetIntValue("wizard_cost"))
        {

        }

        public static string GetInfo()
        {
            return $"Имя: {Settings.GetStringValue("wizard_name")}\n" +
                   $"Вид: {Settings.GetCharValue("wizard_view")}\n" +
                   $"Здоровье: {Settings.GetIntValue("wizard_hp")}\n" +
                   $"Урон: {Settings.GetIntValue("wizard_damage")}\n" +
                   $"Дистанция шага: {Settings.GetIntValue("wizard_stepDistance")}\n" +
                   $"Дистанция атаки: {Settings.GetIntValue("wizard_attackDistance")}\n" +
                   $"Стоимость: {Settings.GetIntValue("wizard_cost")}";
        }

        public static int GetCost()
        {
            return Settings.GetIntValue("wizard_cost");
        }
    }
}
