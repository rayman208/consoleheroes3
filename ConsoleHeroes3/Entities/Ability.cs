﻿namespace ConsoleHeroes3.Entities
{
    class Ability
    {
        private string name;
        private int cost;

        public Ability(string name, int cost)
        {
            this.name = name;
            this.cost = cost;
        }

        public string Name
        {
            get { return name; }
        }
    }
}
