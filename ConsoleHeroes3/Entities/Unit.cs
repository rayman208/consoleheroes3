﻿using System;
using System.Collections.Generic;

namespace ConsoleHeroes3.Entities
{
    abstract class Unit
    {
        private int x, y;
        private string name;
        private int hp;
        private int damage;
        private char view;
        private int stepDistance;
        private int attackDistance;
        private int cost;
        private bool hasSteped;

        private List<Ability> abilities;

        protected Unit(string name, int hp, int damage, char view, int stepDistance, int attackDistance, int cost)
        {
            this.x = 0;
            this.y = 0;
            this.name = name;
            this.hp = hp;
            this.damage = damage;
            this.view = view;
            this.stepDistance = stepDistance;
            this.attackDistance = attackDistance;
            this.cost = cost;
            this.abilities = new List<Ability>();

            this.hasSteped = false;
        }

        public int Cost
        {
            get { return cost; }
        }

        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }
        
        public void StepDone()
        {
            hasSteped = true;
        }

        public void ResetStep()
        {
            hasSteped = false;
        }

        public bool IsAlive
        {
            get { return hp > 0; }
        }

        public void Fight(Unit unit)
        {
            unit.hp -= this.damage;

            //this.hp -= unit.damage; ответочка
        }
        
        public string GetInfo()
        {
            return $"Имя: {name}\n" +
                   $"Вид: {view}\n" +
                   $"Здоровье: {hp}\n" +
                   $"Урон: {damage}\n" +
                   $"Дистанция шага: {stepDistance}\n" +
                   $"Дистанция атаки: {attackDistance}\n" +
                   $"Стоимость: {cost}";
        }

        public string GetShortInfo()
        {
            return $"{name} - {view} X:{x} Y:{y}.\nЗдоровье: {hp}, Урон: {damage}, Дистанция шага: {stepDistance}, Дистанция атаки: {attackDistance}";
        }

        public void Draw(ConsoleColor color)
        {
            if (x == 0 && y == 0) { return; }

            if (hasSteped)
            {
                Console.ForegroundColor = ConsoleColor.DarkGray;
            }
            else
            {
                Console.ForegroundColor = color;
            }

            Console.SetCursorPosition(x, y);
            Console.Write(view);
        }

        public void SetXY(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public bool CanStep(int x2, int y2)
        {
            double distanseCurrentStep = Math.Sqrt(Math.Pow(x2-x, 2) + Math.Pow(y2-y, 2));

            return stepDistance >= distanseCurrentStep;
        }

        public bool CanAttack(int x2, int y2)
        {
            double distanseCurrentAttack = Math.Sqrt(Math.Pow(x2 - x, 2) + Math.Pow(y2 - y, 2));

            return attackDistance >= distanseCurrentAttack;
        }
    }
}
