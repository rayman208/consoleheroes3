﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleHeroes3.Tools;

namespace ConsoleHeroes3.Entities
{
    class Archer : Unit
    {
        public Archer() : base(
            Settings.GetStringValue("archer_name"),
            Settings.GetIntValue("archer_hp"),
            Settings.GetIntValue("archer_damage"),
            Settings.GetCharValue("archer_view"),
            Settings.GetIntValue("archer_stepDistance"),
            Settings.GetIntValue("archer_attackDistance"),
            Settings.GetIntValue("archer_cost"))
        {

        }

        public static string GetInfo()
        {
            return $"Имя: {Settings.GetStringValue("archer_name")}\n" +
                   $"Вид: {Settings.GetCharValue("archer_view")}\n" +
                   $"Здоровье: {Settings.GetIntValue("archer_hp")}\n" +
                   $"Урон: {Settings.GetIntValue("archer_damage")}\n" +
                   $"Дистанция шага: {Settings.GetIntValue("archer_stepDistance")}\n" +
                   $"Дистанция атаки: {Settings.GetIntValue("archer_attackDistance")}\n" +
                   $"Стоимость: {Settings.GetIntValue("archer_cost")}";
        }

        public static int GetCost()
        {
            return Settings.GetIntValue("archer_cost");
        }
    }
}
