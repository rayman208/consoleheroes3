﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleHeroes3.Tools;

namespace ConsoleHeroes3.Entities
{
    class Knight:Unit
    {
        public Knight() : base(
            Settings.GetStringValue("knight_name"),
            Settings.GetIntValue("knight_hp"),
            Settings.GetIntValue("knight_damage"),
            Settings.GetCharValue("knight_view"),
            Settings.GetIntValue("knight_stepDistance"),
            Settings.GetIntValue("knight_attackDistance"),
            Settings.GetIntValue("knight_cost"))
        {

        }

        public static string GetInfo()
        {
            return $"Имя: {Settings.GetStringValue("knight_name")}\n" +
                   $"Вид: {Settings.GetCharValue("knight_view")}\n" +
                   $"Здоровье: {Settings.GetIntValue("knight_hp")}\n" +
                   $"Урон: {Settings.GetIntValue("knight_damage")}\n" +
                   $"Дистанция шага: {Settings.GetIntValue("knight_stepDistance")}\n" +
                   $"Дистанция атаки: {Settings.GetIntValue("knight_attackDistance")}\n" +
                   $"Стоимость: {Settings.GetIntValue("knight_cost")}";
        }

        public static int GetCost()
        {
            return Settings.GetIntValue("knight_cost");
        }
    }
}
