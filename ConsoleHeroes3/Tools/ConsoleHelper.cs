﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleHeroes3.Tools
{
    static class ConsoleHelper
    {
        public static int InputInt(string message)
        {
            bool check;
            int number;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        public static string InputString(string message)
        {
            string str;

            do
            {
                Console.Write(message);
                str = Console.ReadLine();

            } while (str.Length == 0);

            return str;
        }

        public static int InputIntInRange(string message, int min, int max)
        {
            int number;

            do
            {
                number = InputInt(message);
            } while (number < min || number > max);

            return number;
        }

        public static void WaitingEnter()
        {
            Console.WriteLine("Для продолжения нажмите <Enter>");
            Console.ReadKey();
        }
    }
}
