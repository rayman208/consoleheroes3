﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleHeroes3.Tools
{
    static class Settings
    {
        private static Dictionary<string, string> settings;

        public static void Init()
        {
            settings = new Dictionary<string, string>();

            StreamReader reader = new StreamReader("settings.ini");

            while (!reader.EndOfStream)
            {
                string currentLine = reader.ReadLine();
                string[] keyValuePair = currentLine.Split('=');
                settings.Add(keyValuePair[0], keyValuePair[1]);
            }

            reader.Close();
        }

        public static int GetIntValue(string key)
        {
            return int.Parse(settings[key]);
        }

        public static string GetStringValue(string key)
        {
            return settings[key];
        }

        public static char GetCharValue(string key)
        {
            return char.Parse(settings[key]);
        }
    }
}
